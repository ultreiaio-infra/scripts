[![build
status](https://gitlab.com/ultreiaio-infra/scripts/badges/master/pipeline.svg)](https://gitlab.com/ultreiaio-infra/scripts/commits/master)
[![The GNU Lesser General Public License, Version 3.0](https://img.shields.io/badge/license-LGPL3-grren.svg)](http://www.gnu.org/licenses/lgpl-3.0.txt)

# Install

To install scripts, please run this following command:
```bash
wget -q -O - https://gitlab.com/ultreiaio-infra/scripts/raw/master/install.sh | sudo bash
```

This will:

  * Create (or replace) **/usr/local/lib/ultreiaio** directory
  * Download latest scripts inside it 
  * Generate script links into to **/usr/local/bin**

Here is the result of the command:
```bash
Ultreiaio scripts installed in /usr/local/lib/ultreiaio
Install command 'ultreiaio-common'
Install command 'ultreiaio-update'
Install command 'ultreiaio-changelog-update'
Install command 'ultreiaio-changelog-update-staging'
Install command 'ultreiaio-maven-execute'
Install command 'ultreiaio-milestone-close'
Install command 'ultreiaio-milestone-create'
Install command 'ultreiaio-project-create-docker-image'
Install command 'ultreiaio-project-load-maven-repository'
Install command 'ultreiaio-release-finish'
Install command 'ultreiaio-release-gitlab-init'
Install command 'ultreiaio-release-start'
Install command 'ultreiaio-site-gnerate'
Install command 'ultreiaio-site-publish'
Install command 'ultreiaio-stage-close-and-release'
Install command 'ultreiaio-stage-close'
Install command 'ultreiaio-stage-drop'
Install command 'ultreiaio-stage-release'
Install command 'ultreiaio-war-deploy-latest-to-demo'
Install command 'ultreiaio-war-deploy-to-demo'
```