#!/usr/bin/env bash

# to publish site

source $(which ultreiaio-common)

execute_maven "scm-publish:publish-scm" "Publish site"