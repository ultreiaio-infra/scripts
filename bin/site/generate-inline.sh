#!/usr/bin/env bash

# to generate site

source $(which ultreiaio-common)

if [ -f .mvn/mono-module-site ]; then
  MAVEN_COMMAND="clean install site -DperformRelease $@"
else
  MAVEN_COMMAND="clean install site site:stage -DperformRelease $@"
fi
execute_maven "${MAVEN_COMMAND}" "Generate site"
