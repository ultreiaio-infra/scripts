#!/usr/bin/env bash

# to generate site with no build

source $(which ultreiaio-common)

if [ -f .mvn/mono-module-site ]; then
  MAVEN_COMMAND="site -DperformRelease $@"
else
  MAVEN_COMMAND="site site:stage -DperformRelease $@"
fi
execute_maven "${MAVEN_COMMAND}" "Generate site"
