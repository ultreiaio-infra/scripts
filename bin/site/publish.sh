#!/usr/bin/env bash

# to generate site

source $(which ultreiaio-common)

git config --global user.name ${GITLAB_USER_ID}
git config --global user.email ${GITLAB_USER_EMAIL}

execute_maven "scm-publish:publish-scm" "Publish site"