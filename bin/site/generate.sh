#!/usr/bin/env bash

# to generate site

source $(which ultreiaio-common)

execute_maven "clean install -DperformRelease -Dmaven.javadoc.skip" "Build project"

if [ -f .mvn/mono-module-site ]; then
  MAVEN_COMMAND="site -DperformRelease $@"
else
  MAVEN_COMMAND="site site:stage -DperformRelease $@"
fi
execute_maven "${MAVEN_COMMAND}" "Generate site"
