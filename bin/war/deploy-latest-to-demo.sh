#!/usr/bin/env bash

# generate and deploy war to demo

source $(which ultreiaio-common)

MODULE=$1
execute_maven "-Padd-git-commit-id-to-project-version -N" "Fix build version"
execute_maven "clean install -am -pl ${MODULE} -DskipTests -DskipITs" "Prepare latest war"
execute_maven "install -pl ${MODULE} -DskipTests -DskipITs -Pdeploy-war-to-demo-profile -Dclassifier=latest" "Deploy latest war to demo"
git checkout .