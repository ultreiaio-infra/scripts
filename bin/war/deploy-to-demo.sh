#!/usr/bin/env bash

# generate and deploy war to demo

source $(which ultreiaio-common)

MODULE=$1

execute_maven "clean install -am -pl ${MODULE} -DskipTests -DskipITs" "Prepare war"
execute_maven "install -pl ${MODULE} -DskipTests -DskipITs -Pdeploy-war-to-demo-profile" "Deploy war to demo"
