#!/usr/bin/env bash

# to start a release

source $(which ultreiaio-common)

MAVEN_COMMAND="jgitflow:release-start -B -Prelease-profile -e $@"
execute_maven "${MAVEN_COMMAND}" "Start release"
