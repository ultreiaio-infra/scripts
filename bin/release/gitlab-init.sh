#!/usr/bin/env bash

# To init gitlab build fixing git repository

git config --global user.name ${GITLAB_USER_ID}
git config --global user.email ${GITLAB_USER_EMAIL}
git remote set-url --push origin git@${CI_SERVER_HOST}:${CI_PROJECT_PATH}
#git remote remove origin
#git remote add origin git@gitlab.com:${CI_PROJECT_PATH}

source $(which ultreiaio-common)

declare -l DEVELOP
declare -l MASTER

get_gitflow_develop DEVELOP
get_gitflow_master MASTER

echo "fetch all"
git -q fetch --all &>/dev/null
echo "checkout ${DEVELOP}"
git checkout ${DEVELOP} &>/dev/null
git branch --set-upstream-to=origin/${DEVELOP} ${DEVELOP}
echo "checkout ${MASTER}"
git checkout ${MASTER} &>/dev/null
git branch --set-upstream-to=origin/${MASTER} ${MASTER}
echo "back to ${DEVELOP}"
git checkout ${DEVELOP} &>/dev/null
