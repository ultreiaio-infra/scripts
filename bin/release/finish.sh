#!/usr/bin/env bash

# to finish a release

source $(which ultreiaio-common)
MAVEN_COMMAND="jgitflow:release-finish -B -Prelease-profile -e $@"
execute_maven "${MAVEN_COMMAND}" "Finish release"
