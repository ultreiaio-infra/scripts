#!/usr/bin/env bash

# to execute a maven command

COMMAND_ARGS="$1"

source $(which ultreiaio-common)

execute_maven "$COMMAND_ARGS" "Execute mvn $COMMAND_ARGS"
