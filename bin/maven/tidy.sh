#!/usr/bin/env bash

# to sort pom

source $(which ultreiaio-common)

execute_maven "com.github.ekryd.sortpom:sortpom-maven-plugin:sort -Dsort.createBackupFile=false -Dsort.keepBlankLines=false -Dsort.sortDependencies=scope,groupId,artifactId  -Dsort.expandEmptyElements=false" "Tidy and reorder pom"
