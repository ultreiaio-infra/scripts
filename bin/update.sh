#!/usr/bin/env bash

DIR=/usr/local/lib/ultreiaio
BIN_DIR=/usr/local/bin

mkdir -p ${DIR}
rm -rfv ${DIR}/bin

wget -q -O  - "https://gitlab.com/ultreiaio-infra/scripts/-/jobs/artifacts/master/download?job=scripts" > /tmp/scripts.zip
( cd ${DIR} ; unzip -q /tmp/scripts.zip -d . ; rm -rf /tmp/scripts.zip )
echo "Ultreiaio scripts installed in $DIR"

for i in $(ls ${DIR}/bin/*.sh); do
  (cd ${BIN_DIR} ; link=ultreiaio-$(basename ${i} .sh) ; echo "Install command '$link'" ; ln -fs ${i} ${link} );
done

for j in $(ls ${DIR}/bin | grep -v ".sh"); do
  for i in $(ls ${DIR}/bin/${j}/*.sh); do
  (cd ${BIN_DIR} ; link=ultreiaio-$(basename ${j} .sh)-$(basename ${i} .sh) ; echo "Install command '$link'" ; ln -fs ${i} ${link} );
  done
done
