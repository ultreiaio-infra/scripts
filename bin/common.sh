#!/usr/bin/env bash

MAVEN_CLI_OPTS="$MAVEN_CLI_OPTS --show-version -e"

function get_project_id {
  declare -n ret=$1
  echo -n "Get project id..."
  P=$(cat .mvn/pom.projectId)
  echo " $P"
  ret=${P}
}

function get_organization_id {
  declare -n ret=$1
  echo -n "Get organization id..."
  P=$(cat .mvn/pom.organizationId)
  echo " $P"
  ret=${P}
}

function get_gitflow_develop {
  declare -n ret=$1
  echo -n "Get gitflow develop branch name..."
  P=$(cat .mvn/pom.gitflow.develop)
  echo " $P"
  ret=${P}
}

function get_gitflow_master {
  declare -n ret=$1
  echo -n "Get gitflow master branch name..."
  P=$(cat .mvn/pom.gitflow.master)
  echo " $P"
  ret=${P}
}

function get_project_version {
  declare -n ret=$1
  echo -n "Get version..."
  P=$(mvn help:evaluate -N -Dexpression="project.version" | grep -v  "\[" | grep -v "Down")
  P=${P/-SNAPSHOT/}
  echo " $P"
  ret=${P}
}

function execute_maven {
  cmd=$1
  text=$2
  echo "Execute maven ${text}"
  echo "mvn ${MAVEN_CLI_OPTS} ${cmd}"
  mvn ${MAVEN_CLI_OPTS} ${cmd}
  if [ ! "$?" == "0" ]; then
    echo "Error"
    exit 1
  fi
}