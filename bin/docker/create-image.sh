#!/usr/bin/env bash

# Create the base docker image from the given directory

FORGE_REGISTRY=$1
echo "Registry: ${FORGE_REGISTRY}"
PROJECT_PATH=$2
echo "Project path: ${PROJECT_PATH}"
IMAGE_NAME=$3
echo "Image image: ${IMAGE_NAME}"
DIR=$4
echo "Directory containing the docker directory: ${DIR}"
LOGIN=$5
echo "Docker login: ${LOGIN}"
PASSWORD=$6

BUILD_IMAGE=${FORGE_REGISTRY}/${PROJECT_PATH}/${IMAGE_NAME}:main
RELEASE_IMAGE=${FORGE_REGISTRY}/${PROJECT_PATH}/${IMAGE_NAME}:latest

echo "Docker login with ${LOGIN}"
echo "${PASSWORD}" | docker login -u "${LOGIN}" --password-stdin "${FORGE_REGISTRY}"
echo "Docker build image ${BUILD_IMAGE}"
docker build --pull --no-cache -t "${BUILD_IMAGE}" "${DIR}"
echo "Docker tag ${BUILD_IMAGE} to ${RELEASE_IMAGE}"
docker tag "${BUILD_IMAGE}" "${RELEASE_IMAGE}"
echo "Docker push image ${RELEASE_IMAGE}"
docker push "${RELEASE_IMAGE}"
echo "Docker done"
