#!/usr/bin/env bash

LOGIN=$1
PASSWORD=$2

ultreiaio-docker-generate-image-base-11 "${CI_REGISTRY}" "ultreiaio-infra/docker" "${CI_PROJECT_PATH}" "${CI_PROJECT_NAME}" "${LOGIN}" "${PASSWORD}"