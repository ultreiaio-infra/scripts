#!/usr/bin/env bash

FORGE_REGISTRY=$1
DOCKER_PROJECT_PATH=$2
PROJECT_NAMESPACE=$3
PROJECT_NAME=$4
LOGIN=$5
PASSWORD=$6


DIR=/tmp/tmp-docker-${PROJECT_NAMESPACE}-${PROJECT_NAME}
rm -rf "${DIR}"
mkdir -p "${DIR}"
cat << EOF >> "${DIR}"/Dockerfile
FROM ${FORGE_REGISTRY}/${DOCKER_PROJECT_PATH}/base-11:latest
MAINTAINER Tony Chemit <dev@tchemit.fr>
RUN wget -q -O - https://gitlab.com/ultreiaio-infra/scripts/raw/master/install.sh | bash
EOF

ultreiaio-docker-create-image "${FORGE_REGISTRY}" "${CI_PROJECT_PATH}" "${PROJECT_NAME}" docker "${LOGIN}" "${PASSWORD}"