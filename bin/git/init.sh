#!/usr/bin/env bash

# to init git

git config --global user.name ${GITLAB_USER_ID}
git config --global user.email ${GITLAB_USER_EMAIL}
