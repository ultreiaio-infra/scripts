#!/usr/bin/env bash

# to generate changelog

source $(which ultreiaio-common)
command="-N -e gitlab:generate-changelog $@"
execute_maven "${command}" "Generate changelog"
