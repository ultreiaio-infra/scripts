#!/usr/bin/env bash

# to update changelog

source $(which ultreiaio-common)

declare -l VERSION
get_project_version VERSION

execute_maven "-N -e gitlab:generate-changelog" "Updating changelog for v.${VERSION}"

git commit -m "Update changelog for v.${VERSION} [skip CI]" CHANGELOG.md

git push