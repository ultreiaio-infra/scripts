#!/usr/bin/env bash

PROJECT_NAMESPACE=$1
PROJECT_NAME=$2
GIT_DIR=$3

rm -rf ${GIT_DIR}
git clone git@gitlab.com:${PROJECT_NAMESPACE}/${PROJECT_NAME}.git ${GIT_DIR}
cd  ${GIT_DIR}  ; ultreiaio-maven-execute 'clean verify -U -e -DperformRelease -Dmaven.javadoc.skip'
rm -rf  ${GIT_DIR}
