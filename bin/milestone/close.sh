#!/usr/bin/env bash

# to close a milestone

source $(which ultreiaio-common)

declare -l DEVELOP

get_gitflow_develop DEVELOP

git checkout ${DEVELOP} -q

execute_maven "gitlab:close-milestone -N" "Closing gitlab milestone."
